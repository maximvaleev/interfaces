﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces;

internal class Quadcopter : IFlyingRobot, IChargeable
{
    public Quadcopter(string name)
    {
        Name = string.IsNullOrWhiteSpace(name) ? "Some quad" : name;
    }

    public string Name { get; }
    private readonly List<string> _components = new() { "rotor1", "rotor2", "rotor3", "rotor4" };
    
    public void Charge()
    {
        Console.Write("Charging.");
        for (int i = 0; i < 6; i++)
        {
            Thread.Sleep(500);
            Console.Write('.');
        }
        Console.WriteLine("  Charged!");
    }

    public List<string> GetComponents() => _components;

    public string GetInfo() => /*GetRobotType() +*/ " All systems run smoothly.";

    //public string GetRobotType() => "I am a quadcopter!";
}

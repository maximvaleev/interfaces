﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces;

internal interface IRobot
{
    string GetInfo();
    List<string> GetComponents();
    string GetRobotType()
    {
        return "I am a simple robot.";
    }
}

internal interface IFlyingRobot : IRobot 
{
    new string GetRobotType()
    {
        return "I am a flying robot.";
    }
}

internal interface IChargeable
{
    void Charge();
    string GetInfo();
}

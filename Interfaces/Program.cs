﻿/*
  Домашнее задание
*** Интерфейсы ***

Цель:
В этом ДЗ вы научитесь явному вызову интерфейсов, их наследованию и реализации методов по умолчанию.

Описание/Пошаговая инструкция выполнения домашнего задания:

    Создать интерфейс IRobot с публичным методами string GetInfo() и List GetComponents(), а также string GetRobotType() с дефолтной реализацией, возвращающей значение "I am a simple robot.".
    Создать интерфейс IChargeable с методами void Charge() и string GetInfo().
    Создать интерфейс IFlyingRobot как наследник IRobot с дефолтной реализацией GetRobotType(), возвращающей строку "I am a flying robot.".
    Создать класс Quadcopter, наследующий IFlyingRobot и IChargeable. В нём создать список компонентов List _components = new List {"rotor1","rotor2","rotor3","rotor4"} и возвращать его из метода GetComponents().
    Реализовать метод Charge() должен писать в консоль "Charging..." и через 3 секунды "Charged!". Ожидание в 3 секунды реализовать через Thread.Sleep(3000).
    Реализовать все методы интерфейсов в классе. До этого пункта достаточно было "throw new NotImplementedException();"
    В чат напишите также время, которое вам потребовалось для реализации домашнего задания.
 */

namespace Interfaces;

internal class Program
{
    static void Main(string[] args)
    {
        /*
        TestIt(new Quadcopter("IRobot_quad"), typeof(IRobot));
        TestIt(new Quadcopter("IFlyingRobot_quad"), typeof(IFlyingRobot));
        TestIt(new Quadcopter("IChargeable_quad"), typeof(IChargeable));
        TestIt(new Quadcopter("Quadcopter_quad"), typeof(Quadcopter));
        */
        
        Quadcopter quad = new Quadcopter("Quadcopter_quad");
        Console.WriteLine($"\n\t*** Quadcopter_quad ***");
        Console.WriteLine(quad.GetInfo());
        Console.WriteLine(string.Join(' ', quad.GetComponents()));
        quad.Charge();
        Console.WriteLine();

        IRobot robo = new Quadcopter("IRobot_quad");
        Console.WriteLine($"\n\t*** IRobot_quad ***");
        Console.WriteLine(robo.GetInfo());
        Console.WriteLine(robo.GetRobotType());
        Console.WriteLine(string.Join(' ', robo.GetComponents()));
        Console.WriteLine();

        IFlyingRobot roboFly = new Quadcopter("IFlyingRobot_quad");
        Console.WriteLine($"\n\t*** IFlyingRobot_quad ***");
        Console.WriteLine(roboFly.GetInfo());
        Console.WriteLine(roboFly.GetRobotType());
        Console.WriteLine(string.Join(' ', roboFly.GetComponents()));
        Console.WriteLine();

        IChargeable charged = new Quadcopter("IChargeable_quad");
        Console.WriteLine($"\n\t*** IChargeable_quad ***");
        Console.WriteLine(charged.GetInfo());
        charged.Charge();
        Console.WriteLine();
    }
    
    // пытался через рефлексию, но при таком вызове не работает наследование интерфейсов
    private static void TestIt(Quadcopter quad, Type type)
    {
        Console.WriteLine($"\n\t*** {quad.Name} *** ({type})");
        try
        {
            Console.WriteLine("Trying to invoke GetInfo()...");
            Console.WriteLine($"\t{type.GetMethod("GetInfo").Invoke(quad, null)}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"\t{ex.Message}");
        }

        try
        {
            Console.WriteLine("Trying to invoke GetComponents()...");
            Console.WriteLine($"\t{string.Join(", ", (List<string>)type.GetMethod("GetComponents").Invoke(quad, null))}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"\t{ex.Message}");
        }

        try
        {
            Console.WriteLine("Trying to invoke GetRobotType()...");
            Console.WriteLine($"\t{type.GetMethod("GetRobotType").Invoke(quad, null)}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"\t{ex.Message}");
        }

        try
        {
            Console.WriteLine("Trying to invoke Charge()...");
            type.GetMethod("Charge").Invoke(quad, null);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"\t{ex.Message}");
        }
    }
}